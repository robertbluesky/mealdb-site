import React, { Component} from 'react'
import axios from 'axios'
import { IMealResponse, IMeal } from '../interfaces/interfaces'
import MealSearchResult from './MealSearchResult'
import Pagination from './Pagination'
var Spinner = require('react-spinkit')

interface IProps {
    searchValue: string,
    formSubmitted: boolean,
    resetSubmit: ()=>void,
    searchType: string
}

interface IState {
    meals: IMeal[],
    loading: boolean,
    currentPage: number,
    mealsPerPage: number,
    searched: boolean
}

export class SearchResults extends Component<IProps, IState> {
    state = {
        meals: [] as IMeal[],
        loading: false,
        mealsPerPage: 5,
        searched: false,
        currentPage: 1
    }

    async componentDidUpdate(prevProps: IProps, prevState: IState,) {
        if ((prevProps.formSubmitted !== this.props.formSubmitted) && this.props.formSubmitted) {
            try {
                this.setState({loading: true, searched: true})
                this.props.resetSubmit()
                let res: any;
                if (this.props.searchType === "name") {
                    res = await axios.get<IMealResponse>(`https://www.themealdb.com/api/json/v1/1/search.php?s=${this.props.searchValue}`)
                } else if (this.props.searchType === "category") {
                    res = await axios.get<IMealResponse>(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${this.props.searchValue}`)
                }
                this.setState({loading: false})
                if (res.data.meals === null) {
                    this.setState({meals: [] as IMeal[]})
                } else {
                    this.setState({meals: res.data.meals})
                }
            } catch(e) {
                console.error(e)
            }
        }
    }

    paginate = (number: number) => {
        this.setState({currentPage: number})
    }

    setMealsPerPage = (amount: number) => {
        this.setState({mealsPerPage: amount})
    }

    render() {
        const {searched, meals, mealsPerPage, currentPage} = this.state

        // Calculate recipe results based on page limit
        const indexOfLastRecipe = currentPage * mealsPerPage
        const indexOfFirstRecipe = indexOfLastRecipe - mealsPerPage
        const currentMeals = meals.slice(indexOfFirstRecipe, indexOfLastRecipe)

        if (this.state.loading) {
            return (
                <div className="spinner"><Spinner fadeIn='none' name="line-scale-pulse-out" color="steelblue"/></div>
            )
        } else if (this.state.meals.length > 0) {
            return (
                <div className="container">
                    {currentMeals.map(m => <MealSearchResult key={m.idMeal} meal={m} />)}
                    <Pagination mealsPerPage={mealsPerPage} totalMeals={meals.length} paginate={this.paginate} setMealsPerPage={this.setMealsPerPage}/>
                </div>
            )
        } else if (searched) {
            return (
                <p>Could not find any recipes...</p>
            )
        } else {
            return (
                <p></p>
            )
        }
    }
}

export default SearchResults