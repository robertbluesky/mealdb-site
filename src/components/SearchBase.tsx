import React, { Component } from 'react'
import {
    Divider,
    Grid,
    Header,
    Icon,
    Segment,
  } from 'semantic-ui-react'
  import SearchByName from './SearchByName'
import SearchByCategory from './SearchByCategory'

interface IProps {
    updateSearchValue: (newValue: string)=>void
    submitForm: ()=>void
    resetSubmit: ()=>void
    updateSearchType: (type: string)=>void
    searchValue: string
    formSubmitted: boolean
    searchType: string
}

export class SearchBase extends Component<IProps> {

    render() {
        return (
            <Segment placeholder>
                <Grid columns={2} stackable textAlign='center'>
                <Divider vertical>Or</Divider>

                <Grid.Row verticalAlign='middle'>
                    <Grid.Column>
                    <Header icon>
                        <Icon name='sort alphabet down' />
                        Search by name
                    </Header>
                    <SearchByName updateSearchValue={this.props.updateSearchValue} searchValue={this.props.searchValue} submitForm={this.props.submitForm} updateSearchType={this.props.updateSearchType} searchType={this.props.searchType}/>
                    </Grid.Column>

                    <Grid.Column>
                    <Header icon>
                        <Icon name='list layout' />
                        Search by category
                    </Header>
                    <SearchByCategory updateSearchType={this.props.updateSearchType} updateSearchValue={this.props.updateSearchValue} submitForm={this.props.submitForm} searchType={this.props.searchType}/>
                    </Grid.Column>
                </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

export default SearchBase
