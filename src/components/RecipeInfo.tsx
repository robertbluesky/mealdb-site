import React, { Component } from 'react'
import axios from 'axios'
import { IMealResponse, IMeal } from '../interfaces/interfaces'
import Select from 'react-select'
import {
    Button,
    Icon,
    Segment,
} from 'semantic-ui-react'

var Spinner = require('react-spinkit')

interface IProps {
    id: string
}

interface IState {
    meal: IMeal
    loading: boolean
    ingredientCompound: string[]
    instructions: string
    sentences: string[]
    batches: number
}

export class RecipeInfo extends Component<IProps, IState> {
    state = {
        meal: {} as IMeal,
        loading: true,
        ingredientCompound: [] as string[],
        instructions: "",
        sentences: [] as string[],
        batches: 1
    }

    parseIngredients = () => {
        let names = [
            this.state.meal.strIngredient1,
            this.state.meal.strIngredient2,
            this.state.meal.strIngredient3,
            this.state.meal.strIngredient4,
            this.state.meal.strIngredient5,
            this.state.meal.strIngredient6,
            this.state.meal.strIngredient7,
            this.state.meal.strIngredient8,
            this.state.meal.strIngredient6,
            this.state.meal.strIngredient10,
            this.state.meal.strIngredient11,
            this.state.meal.strIngredient12,
            this.state.meal.strIngredient13,
            this.state.meal.strIngredient14,
            this.state.meal.strIngredient15,
            this.state.meal.strIngredient16,
            this.state.meal.strIngredient17,
            this.state.meal.strIngredient18,
            this.state.meal.strIngredient19,
            this.state.meal.strIngredient20
        ]
        names = names.filter(value => value !== "" && value !== null)

        let measures = [
            this.state.meal.strMeasure1,
            this.state.meal.strMeasure2,
            this.state.meal.strMeasure3,
            this.state.meal.strMeasure4,
            this.state.meal.strMeasure5,
            this.state.meal.strMeasure6,
            this.state.meal.strMeasure7,
            this.state.meal.strMeasure8,
            this.state.meal.strMeasure6,
            this.state.meal.strMeasure10,
            this.state.meal.strMeasure11,
            this.state.meal.strMeasure12,
            this.state.meal.strMeasure13,
            this.state.meal.strMeasure14,
            this.state.meal.strMeasure15,
            this.state.meal.strMeasure16,
            this.state.meal.strMeasure17,
            this.state.meal.strMeasure18,
            this.state.meal.strMeasure19,
            this.state.meal.strMeasure20
        ]
        measures = measures.filter(value => value !== "" && value !== null)
        
        let ingrComp = [] as string[];
        for (let i = 0; i < names.length; i++) {
            ingrComp[i] = names[i] + ": " + measures[i]
        }
        this.setState({ingredientCompound: ingrComp})
    }

    multiplyMeasure = (measure: number) => {
        // TODO
    }

    splitInstructions = () => {
        if (this.state.meal.strInstructions !== undefined) {
            const sentences = this.state.meal.strInstructions.split(". ")

            this.setState({
                sentences: sentences
            })
        }
    }

    // TODO: change from any to something better
    updateBatches = (event: any) => {
        this.setState({batches: event.value})
    }

    async componentDidMount() {
        try {
            const res = await axios.get<IMealResponse>(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${this.props.id}`)
            this.setState({meal: res.data.meals[0], loading: false})
            this.parseIngredients();
            this.splitInstructions()
        } catch(e) {
            console.error(e)
        }
    }

    previousRecipe = (event: React.MouseEvent<HTMLButtonElement>) => {
        //TODO: route to new recipe page
    }

    render() {
        const {meal, loading} = this.state

        const options = [
            { value: 1, label: '1 Batch' },
            { value: 2, label: '2 Batches' },
            { value: 3, label: '3 Batches' },
            { value: 4, label: '4 Batches' }
          ]

        if (loading) {
            return (
                <div><div className="spinner"><Spinner fadeIn='none' name="line-scale-pulse-out" color="steelblue"/></div></div>
            )
        }
        else {
            return (
                <div>
                    <h1>{meal.strMeal}</h1>
                    <img className="meal-thumb" src={meal.strMealThumb} alt="meal thumb" />
                    <h3 className="ingredients-header">Ingredients:</h3>
                    <div className="batch-dropdown-div">
                        <Select className="batch-dropdown" defaultValue={options[0]} onChange={this.updateBatches} options={options} />
                    </div>
                    <ul className="ingredient-list">
                        {this.state.ingredientCompound.map((name, i) => <li className="ingredient-list-item" key={i}>{name}</li>)}
                    </ul>
                    <h3 className="instructions-header">Instructions:</h3>
                    <div className="instructions">
                        {this.state.sentences.map(sentence => 
                            <p key={sentence}>{sentence}.<br/></p>
                        )}
                    </div>
                    <Segment>
                        <Button animated="vertical" onClick={this.previousRecipe}>
                            <Button.Content visible>Previous Recipe</Button.Content>
                            <Button.Content hidden>
                                <Icon name='arrow left' />
                            </Button.Content>
                        </Button>

                        <Button animated='vertical' onClick={() => {}}>
                            <Button.Content visible>Next Recipe</Button.Content>
                            <Button.Content hidden>
                                <Icon name='arrow right' />
                            </Button.Content>
                        </Button>
                    </Segment>
                </div>
            )
        }
    }
}

export default RecipeInfo