import React, { Component } from 'react'
import Select from 'react-select'
import axios from 'axios'
import { ICategoryResponse, ICategory} from '../interfaces/interfaces'

interface IProps {
    updateSearchType: (type: string)=>void
    updateSearchValue: (value: string)=>void
    submitForm: ()=>void
    searchType: string
}

interface IState {
    categories: ICategory[]
}

export class SearchByCategory extends Component<IProps> {
    state = {
        categories: [] as ICategory[]
    }

    async loadCategories() {
        try {
          const res = await axios.get<ICategoryResponse>('https://www.themealdb.com/api/json/v1/1/categories.php')
          this.setState({categories: res.data.categories})
        } catch(e) {
            console.error(e)
        }
    }

    // TODO: change from any to something better
    updateSearch = (event: any) => {
        this.props.updateSearchType("category")
        this.props.updateSearchValue(event.value)
        this.props.submitForm()
    }


    componentDidMount() {
        this.loadCategories()
    }

    render() {
        const options = this.state.categories.map(category => {return {value:category.strCategory, label:category.strCategory}})

        return (
            <div>
                <Select  options={options} onChange={this.updateSearch}/>
            </div>
        )
    }
}

export default SearchByCategory