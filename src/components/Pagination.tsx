import React, { Component } from 'react'

interface IProps {
   mealsPerPage: number,
   totalMeals: number,
   paginate: (number: number)=>void,
   setMealsPerPage: (amount: number)=>void
}

export class Pagination extends Component<IProps> {
    render() {
        const {mealsPerPage, totalMeals} = this.props

        const pageNumbers = [] as number[];

        for (let i = 1; i <= Math.ceil(totalMeals / mealsPerPage); i++) {
            pageNumbers.push(i);
        }

        return (
            <nav>
                <ul className="pagination">
                    {pageNumbers.map(number => (
                        <li key={number} className="page-item">
                            <button type="button" onClick={() => this.props.paginate(number)} className="page-link">
                                {number}
                            </button>
                        </li>
                    ))}
                </ul>
                <h4>Results per page:</h4>
                <select onChange={(event) => this.props.setMealsPerPage(Number(event.target.value))} defaultValue={this.props.mealsPerPage} className="results-per-page">
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                </select>
            </nav>
        )
    }
}

export default Pagination
