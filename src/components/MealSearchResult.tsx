import React, { Component } from 'react'
import { IMeal } from '../interfaces/interfaces'
import { Link } from 'react-router-dom'

interface IProps {
    meal: IMeal
}

interface IState {
    
}

export class MealSearchResult extends Component<IProps, IState> {
    render() {
        const {meal} = this.props

        return (
            <div>
                <Link to={`/singlerecipe/${meal.idMeal}`}>
                    <img className="meal-thumb" src={meal.strMealThumb} alt="recipe"/>
                </Link>
                    <p className="meal-thumb-text" >{meal.strMeal}</p>
            </div>
        )
    }
}

export default MealSearchResult
