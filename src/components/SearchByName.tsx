import React, { Component } from 'react'
import { Input } from 'semantic-ui-react'

interface IProp {
    updateSearchValue: (newValue:string)=>void
    searchValue: string
    submitForm: ()=>void
    updateSearchType: (type: string)=>void
    searchType: string
}

export class SearchByName extends Component<IProp> {
    changeInputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.updateSearchValue(event.target.value);
        this.props.updateSearchType("name")
    }

    onSubmitHandler = (event: React.FormEvent<HTMLFormElement>) =>{
        event.preventDefault()
        this.props.submitForm()
    }

    render() {
        let textDisplay: string;

        if (this.props.searchType === "name") {
            textDisplay = this.props.searchValue
        } else {
            textDisplay = ""
        }

        return (
            <form onSubmit={this.onSubmitHandler}>
                <Input size='huge' required value={textDisplay} onChange={this.changeInputHandler} icon='search' placeholder='Search...'/>
            </form>
        )
    }
}

export default SearchByName