import React, { Component } from 'react';
import './App.css';
import SearchResults from './components/SearchResults';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import RecipeInfo from './components/RecipeInfo';
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBase from './components/SearchBase';

interface IState {
  searchValue: string
  formSubmitted: boolean
  searchType: string
}

export class App extends Component<{}, IState> {
  state = {
    searchValue: "",
    formSubmitted: false,
    searchType: ""
  }

  updateSearchValue = (newValue: string) => {
    this.setState({searchValue: newValue})
  }

  submitForm = () => {
    this.setState({formSubmitted: true})
  }

  resetSubmit = () => {
    this.setState({formSubmitted: false})
  }

  updateSearchType = (type: string) => {
    this.setState({searchType: type})
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Route path="/" exact>  
            <h1>Welcome to the Recipe Database</h1>
            <SearchBase searchType={this.state.searchType} formSubmitted={this.state.formSubmitted} searchValue={this.state.searchValue} updateSearchValue={this.updateSearchValue} submitForm={this.submitForm} resetSubmit={this.resetSubmit} updateSearchType={this.updateSearchType} />
            <SearchResults searchValue={this.state.searchValue} formSubmitted={this.state.formSubmitted} resetSubmit={this.resetSubmit} searchType={this.state.searchType} />
          </Route>
          <Route path="/singlerecipe/:id" render={({match})=>(
              <RecipeInfo id={match.params.id} />
          )} />
        </div>
      </Router>
    );
  }
}

export default App;